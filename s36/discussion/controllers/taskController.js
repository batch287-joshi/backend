// Contains instruction on HOW your API will perform its intended tasks
// All of the operations it can be done will be placed in this file

const Task = require("../models/task");

// Controller for getting all the tasks
// Defines the function to be used in the "taskRoute.js" file and export these functions

module.exports.getAllTasks = () => {

	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoutes.js"
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to our client/postman

	return Task.find({}).then(result => {
		// The 'return' statement returns the result of the MongoDB query to the result parameter defined in the "then" method.
		return result;
	});
};

module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {

		if(error){
			console.log(error);
			return false
		} else {
			return task
		};
	});
};

// Controller deleting a task

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err){
			console.log(err)
			return false

		} else {
			return `"${taskId}" is now deleted`
		}
	})
}

// Controller updating a task

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false

		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {

			if(saveErr){

				console.log(saveErr);
				return false
			} else {

				return "Task updated.";
			};
		});
	});
};