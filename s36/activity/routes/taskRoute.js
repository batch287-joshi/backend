const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");

router.get("/:id", (req, res) => {

	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Update task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;