const Task = require("../models/task");

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	});
};

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		}
		result.status = newContent.status;
		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false
			} else {
				return "Task updated.";
			};
		});
	});
};

module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {

		if(error){
			console.log(error);
			return false
		} else {
			return task
		};
	});
};