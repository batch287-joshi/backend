let number = prompt("Please input a number");

console.log("The number you provided is " + number + ".");
for(i = number; i > 0; i--){
	if(i <= 50){
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}
	else if(i % 10 == 0){
		console.log("The number is divisible by 10. Skipping the number");
		continue;
	}
	else if(i % 5 == 0){
		console.log(i);
	};
};

let longString = "supercalifragilisticexpialidocious";
let longStringConsonants = "";

	for(let i = 0; i < longString.length; i++){

		// i = a value of number
		if( 
			!(longString[i].toLowerCase() === 'a' || 
			longString[i].toLowerCase() === 'i' ||
			longString[i].toLowerCase() === 'u' ||
			longString[i].toLowerCase() === 'e' ||
			longString[i].toLowerCase() === 'o') 
		  ){
			longStringConsonants += longString[i];
		};
	};

console.log(longString);
console.log(longStringConsonants);