// Cube of a number

function Cube(num){
	return Math.pow(num,3);
};
let num = 2;
let getCube = Cube(num);
console.log(`The cube of ${num} is ${getCube}`);


// Full address

let address = ["258 Washington Ave NW", "California", "90011"];
const [street, state, pinCode] = address;
console.log(`I live at ${street}, ${state} ${pinCode}`);


// Animal and its characteristics

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	height: "20 ft 3 in"
};
const {name, type, weight, height} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${height}.`);

// Array of numbers

numbers = [1, 2, 3, 4, 5];
let reduceNumber = 0;
numbers.forEach((number) => {
	console.log(number);
	reduceNumber += number;
});
console.log(reduceNumber);


// Class Dog

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};
let dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);