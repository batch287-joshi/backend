console.log("Hello World");

// [ SECTION ] Functions

	// [ SUB-SECTION ] Parameters and Arguments

	// Functions in JS are lines/blocks of codes that tell our device/aplication/browser to perform a certain task when called/invoked/triggered.

	function printInput() {
		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname);
	};

	// printInput();

	// For other case, functions can also process data directly passed into it instead of relying only on Global Variable and prompt().

	// name ONLY ACTS LIKE A VARIABLE

	function printName(name) {// name is the parameter

		// This one should work as well:
			// let name1233312 = name;
			// console.log("Hi, " + name1233312);

		console.log("Hi, " + name);
	};

	// console.log(name); // This one wil give an error as it is not defined.

	printName("Juana"); // "Juana" is the argument.

	// You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

	printName("Carlo");

	// When the "printName()" function is called again, it stores the value of "Carlo" in the parameter "name" then uses it to print a message.

	// Variables can also be passed as an argument.
	let sammpleVariable = "Yui";

	printName(sammpleVariable);

	// Function arguments cannot be used by a function if there are no parameters provided within the function.

	printName(); // It will give 'Hi, undefined'

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8? ");
		console.log(isDivisibleBy8);
	};

	checkDivisibilityBy8(16);
	checkDivisibilityBy8(52);

	// You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

// Functions as Arguments

		// Function parameters can also accept other function as arguments 

		function argumentFunction() {
			console.log("This function was passed as an argument before the message was printed.");
		};

		// function invokeFunction(argumentFunction) {
		// 	argumentFunction();
		// };

		function invokeFunction(iAmNotRelated) {
			iAmNotRelated();
			argumentFunction();
		};

		invokeFunction(argumentFunction);

		// Adding and removing the parentheses "()" impacts the output of JS heavily.

		console.log(argumentFunction);
		// Will provide information about a function in the console using console.log();

	// Using multiple parameters 

		// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

		function createFullName(firstName, middleName, lastName) {
			console.log(firstName + " " + middleName + " " + lastName);
		};

		createFullName("Mike", "Kel", "Jordan");

		createFullName("Mike", "Jordan");

		createFullName("Mike", "Kel", "Jordan", "Mike");

		// In JS, providing more/less arguments than the expected parameters will not return an error.

		// Using Variable as Arguments

		let firstName = "Dwayne";
		let secondName = "Rock";
		let lastName = "Johnson";

		createFullName(firstName, secondName, lastName);

// [ SECTION ] The Return Statement

	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.
	
		function returnFullName(firstName, middleName, lastName) {
			console.log("This messsage is from console.log");
			return firstName + " " + middleName + " " + lastName;
			console.log("This messsage is from console.log");
			console.log("This messsage is from console.log");
		};

		let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
		console.log(completeName);

		// In our example, the "returnFullName" function was invoked called in the same line as declaring a variable.

		// Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable.

		// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.

		// In this example, comsole.log() will print the returned value of the returnFullName() function. 
		console.log(returnFullName(firstName, secondName, lastName));

		function returnAddress(city, country) {
			let fullAddress = city + " , " + country;
			return fullAddress;
		};

		let myAddress = returnAddress("Manila City" , "Philippines");
		console.log(myAddress);

		function printPlayerInfo(username, level, job) {
			console.log("Username: " + username);
			console.log("Level: " + level);
			console.log("Job" + job);
		};

		let user1 = printPlayerInfo("knight_white", 95, "Paladin");
		console.log(user1);

		// Returns undefined because printPlayerInfo returns nothing. It just console.log the details.

		// You cannot save any value from printPlayerInfo() because it does not return anything.