const express = require("express");

const app = express();

const port = 3000;

app.use(express.json())


// GET home page
app.get("/home", (request, response) => {
	response.send('Welcome to the home page');
})

// GET users data
let users = [];

users.push({
	"username": "johndoe",
	"password": "johndoe1234" 
})

app.get("/users", (request, response) => {
	console.log(users);
	response.send(users);
})

//DELETE user
app.delete("/delete-user", (request,response) => {
	users.pop(request.body);
	response.send(`User ${request.body.username} has been deleted.`);
	console.log(users);
})

app.listen(port, () => console.log(`Server is currently running at port ${port}`));