// Use the require directive to load the express module/package
// It allows us to access to methods and functions that will allow us to easily create a server
const express = require("express");

// Creates an application using express
// In layman's term, app is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 3001;

// Use middleware to allow express to read JSON
app.use(express.json())

// Use middleware to allows express to able to read more data types from a response
app.use(express.urlencoded({extended: true}));

// [ SECTION ] Routes
	// Express has methods corresponding to each HTTP method

app.get("/hello", (request, response) => {
	response.send('Hello from the /hello endpoint!');
})

app.post("/display-name", (req, res) => {
	console.log(req.body);
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// Sign-up in our Users.

let users = [];; // This is our mockdatabase

app.post("/signup", (request, response) => {
	console.log(request.body)

	if(request.body.username != "" && request.body.password != "") {
		response.send(`User added`);
		users.push(request.body);
		console.log(users);
	}
	else {
		response.send(`Valid username and password required`);
	}
});


// This route expects to receive a PUT request at the URL "/change-password"

app.put("/change-password", (req, res) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated.`;

			console.log("Newly updates mock database:");

			console.log(users);

			break;
		} else {
			message = "User does not exist."
		} 
	}

	res.send(message);
})

app.listen(port, () => console.log(`Server is currently running at port ${port}`));